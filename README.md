# Bisect pipeline

This bisect pipeline will be triggered when a kernel build from tuxsuite fails.

The script can run stand alone too.  There's one script to find an old (good)
commit and the other script runs the bisection.  The bisection will be done
with tuxmake in one build-dir to speed up the bisection. When the faulty commit
has been found, the bisect.sh script will try to revert that patch and build.
